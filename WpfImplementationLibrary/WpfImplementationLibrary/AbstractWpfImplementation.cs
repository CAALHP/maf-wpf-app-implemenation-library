﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using CAALHP.Contracts;

namespace WpfImplementationLibrary
{
    public abstract class AbstractWpfImplementation : IAppCAALHPContract
    {
        private Thread _thread;
        protected volatile Application _app;
        private IHostCAALHPContract _host;
        private int _processId;

        #region Abstract Methods

        public abstract string GetName();
        public abstract void Show();
        
        #endregion

        #region Implemented Methods

        protected AbstractWpfImplementation()
        {
            InitThread();
            while (_app == null) { }
            
        }
        
        protected void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

        /// <summary>
        /// used if subscribed to events
        /// </summary>
        /// <param name="notification"></param>
        public void Notify(KeyValuePair<string, string> notification)
        {
            // Implement this to send events to the system
        }


        /// <summary>
        /// called by the host, to initialize the plugin.
        /// </summary>
        /// <param name="hostObj"></param>
        /// <param name="processId"></param>
        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.SubscribeToEvents(_processId);
        }

        public IList<IAppInfo> GetListOfInstalledApps()
        {
            //throw new System.NotImplementedException();
            return new List<IAppInfo>();
        }

        protected void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                /*SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));*/
                //DispatcherHelper.Initialize();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        #endregion
    }
}